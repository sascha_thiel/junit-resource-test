package com.example.junitresourcetest.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Sascha Thiel
 */
@RestController
@RequestMapping("/foo")
public class FooController {

    @PreAuthorize("hasAuthority('SCOPE_test:read')")
    @GetMapping
    public ResponseEntity<Object> listAll() {
        return ResponseEntity.ok(List.of("foo1", "foo2", "foo3", "foo4"));
    }

}
