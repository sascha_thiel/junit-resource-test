package com.example.junitresourcetest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JunitResourceTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(JunitResourceTestApplication.class, args);
    }

}
